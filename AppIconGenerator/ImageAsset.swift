//
//  ImageAsset.swift
//  AppIconGenerator
//
//  Created by Taylor Ryan on 2/6/17.
//  Copyright © 2017 com.trifecta.tr. All rights reserved.
//

import Foundation

struct ImageAsset {
    
    var idiom: String?
    var size: String?
    var filename: String?
    var scale: String?
    
    /**
     Returns new instance created from provided JSON.
     */
    public init?(json: [String:Any]) {
        self.idiom = json["idiom"] as? String
        self.size = json["size"] as? String
        self.filename = json["filename"] as? String
        self.scale = json["scale"] as? String
    }
    
    /**
     Encodes and object as JSON.
     */
    public func toJSON() -> [String:Any]? {
        
        let json: [String:Any] = [
            "idiom": self.idiom ?? "",
            "size": self.size ?? "",
            "filename": self.filename ?? "",
            "scale": self.scale ?? ""
        ]
        
        return json
    }
    
    public func getSize() -> Double {
        guard let size = size else { return 0.0 }
        
        if let sizeString = size.components(separatedBy: "x").first {
            return Double(sizeString)!
        }
        
        return 0.0
    }
    
    public func getScale() -> Int {
        guard let scale = scale else { return 1 }
        
        if let scaleString = scale.components(separatedBy: "x").first {
            return Int(scaleString)!
        }
        
        return 0
    }
}
