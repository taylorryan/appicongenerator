//
//  Generator.swift
//  AppIconGenerator
//
//  Created by Taylor Ryan on 2/3/17.
//  Copyright © 2017 com.trifecta.tr. All rights reserved.
//

import Cocoa
import AVFoundation

enum BuildType: String {
    case beta = "beta"
    case demo = "demo"
    case dev = "dev"
    case qa = "qa"
    case release = "release"
    case sit = "sit"
    case uat = "uat"
    
    init(value: String) {
        switch value {
        case "beta": self = .beta
        case "demo": self = .demo
        case "dev": self = .dev
        case "qa": self = .qa
        case "ci": self = .qa
        case "release": self = .release
        case "sit": self = .sit
        case "uat": self = .uat
        default: self = .release
        }
    }
    
    func color() -> NSColor {
        switch self {
        case .beta:
            return NSColor.black
        case .demo:
            return NSColor(red: 214/255, green: 210/255, blue: 45/255, alpha: 1.0)
        case .dev:
            return NSColor(red: 121/255, green: 139/255, blue: 45/255, alpha: 1.0)
        case .qa:
            return NSColor(red: 40/255, green: 120/255, blue: 179/255, alpha: 1.0)
        case .release:
            return NSColor.clear
        case .sit:
            return NSColor(red: 220/255, green: 124/255, blue: 43/255, alpha: 1.0)
        case .uat:
            return NSColor(red: 109/255, green: 18/255, blue: 62/255, alpha: 1.0)
        }
    }
}

class Generator {
    
    var type: BuildType
    let sourceString: String
    let assetsString: String
    var sourceURL: URL!
    var assetsURL: URL!
    var sourceImage: NSImage!
    var assetFile: AssetFile!
    
    init(source: String, assets: String, type: BuildType = .release) {
        self.type = type
        self.sourceString = source
        self.assetsString = assets
        
        sourceURL = URL(fileURLWithPath: sourceString)
        self.sourceImage = NSImage(contentsOf: sourceURL)!
        
        try? parseAssets()
    }
    
    func parseAssets() throws {
        
        let path = "\(assetsString)/AppIcon.appiconset/Contents.json"
        assetsURL = URL(fileURLWithPath: path)
        let data = try? Data(contentsOf: assetsURL)
        
        if let data = data {
            let json = try JSONSerialization.jsonObject(with: data) as! [String: Any]
            assetFile = AssetFile(json: json)
        }
    }
    
    func clearFolder() throws {
        let fileManager = FileManager.default
        
        let files = try fileManager.contentsOfDirectory(atPath: "\(assetsString)/AppIcon.appiconset")
        
        files.forEach {
            if !$0.contains("Contents.json") {
                try? fileManager.removeItem(atPath: "\(assetsString)/AppIcon.appiconset/\($0)")
            }
        }
    }
    
    func generate() throws {
        
        try self.clearFolder()
        
        self.assetFile.images = self.assetFile.images.map {
            var image = $0
            image.filename = self.generate(imageAsset: $0)
            return image
        }
        
        let data = try JSONSerialization.data(withJSONObject: self.assetFile!.toJSON() as Any, options: .prettyPrinted)
        
        let path = "\(assetsString)/AppIcon.appiconset/Contents.json"
        let assetsURL = URL(fileURLWithPath: path)
        try data.write(to: assetsURL)
    }
    
    func generate(imageAsset:ImageAsset) -> String {
        
        let width = imageAsset.getSize()
        let scale = imageAsset.getScale()
        
        var image = sourceImage.copy() as! NSImage
        
        addText(image)
        
        image = self.scale(image, width: width, scale: Double(scale))
        
        let tiffData = image.tiffRepresentation
        let bitmapRep = NSBitmapImageRep(data: tiffData!)
        let imageData = bitmapRep!.representation(using: .PNG, properties: [:])
        
        var filename = sourceURL.lastPathComponent
        filename = filename.substring(to: filename.index(filename.endIndex, offsetBy: -4))
        let outputURL = URL(fileURLWithPath: assetsString)
            .appendingPathComponent("AppIcon.appiconset")
            .appendingPathComponent("\(filename)-\(type.rawValue)-\(width)@\(scale)x", isDirectory: false)
            .appendingPathExtension("png")

        do {
            try imageData?.write(to: outputURL)
        } catch {
            print("Error: Could not save image for \(type.rawValue), size \(width)@\(scale)x ")
        }
        
        return outputURL.lastPathComponent
    }
    
    func addText(_ image: NSImage) {
        let size = image.size
        
        let rep = NSBitmapImageRep.init(bitmapDataPlanes: nil,
                                        pixelsWide: Int(size.width),
                                        pixelsHigh: Int(size.height),
                                        bitsPerSample: 8,
                                        samplesPerPixel: 4,
                                        hasAlpha: true,
                                        isPlanar: false,
                                        colorSpaceName: NSCalibratedRGBColorSpace,
                                        bytesPerRow: 0,
                                        bitsPerPixel: 0)
        
        image.addRepresentation(rep!)
        image.lockFocus()
        
        if type != .release {
            let rect = NSMakeRect(0, 0, size.width, size.height/5)
            
            let ctx = (NSGraphicsContext.current()?.cgContext)!
            ctx.clear(rect)
            
            
            ctx.setFillColor(type.color().cgColor)
            ctx.fill(rect)
            
            
            let originalString: NSString = type.rawValue.uppercased() as NSString
            let spaceToFill = (size.height/5)
            
            var fontSize = 30.0
            var font:NSFont
            var stringSize: CGSize = .zero
            
            repeat {
                fontSize += 1
                font = NSFont.boldSystemFont(ofSize: CGFloat(fontSize))
                stringSize = originalString.size(withAttributes: [NSFontAttributeName: font])
            } while stringSize.height < spaceToFill && stringSize.width < (size.width * 0.8)
            
            let text: NSAttributedString = NSAttributedString(string: type.rawValue.uppercased(), attributes: [NSForegroundColorAttributeName: NSColor.white, NSFontAttributeName: font])
            
            let textRect = NSMakeRect((rect.width - stringSize.width)/2, (rect.height - stringSize.height)/2, stringSize.width, stringSize.height)
            
            text.draw(in: textRect)
        }
        
        image.unlockFocus()
    }
    
    func scale(_ image: NSImage, width: Double, scale: Double = 1.0) -> NSImage  {
        
        let finalWidth = (width/2) * scale
        
        let boundingRect = CGRect(x: 0, y: 0, width: finalWidth, height: finalWidth)
        let rect = AVMakeRect(aspectRatio: image.size, insideRect: boundingRect)
        
        let img = NSImage(size: rect.size)
        img.lockFocus()
        let ctx = NSGraphicsContext.current()
        ctx?.imageInterpolation = .high
        image.draw(in: rect, from: CGRect(origin: CGPoint.zero, size: image.size), operation: .copy, fraction: 1)
        img.unlockFocus()

        return img
        
    }
    
}
