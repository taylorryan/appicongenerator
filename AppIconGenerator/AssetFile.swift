//
//  AssetFile.swift
//  AppIconGenerator
//
//  Created by Taylor Ryan on 2/6/17.
//  Copyright © 2017 com.trifecta.tr. All rights reserved.
//

import Foundation

struct AssetFile {
    
    var images: [ImageAsset]
    
    /**
     Returns new instance created from provided JSON.
     */
    public init?(json: [String:Any]) {
        let array = json["images"] as! [[String:Any]]
        self.images = array.flatMap { ImageAsset(json: $0) }
    }

    /**
     Encodes and object as JSON.
     */
    public func toJSON() -> [String:Any]? {
        
        let jsonArray = images.flatMap { $0.toJSON() }
    
        return [
            "images": jsonArray
        ]
    }
}
