//
//  main.swift
//  AppIconGenerator
//
//  Created by Taylor Ryan on 2/3/17.
//  Copyright © 2017 com.trifecta.tr. All rights reserved.
//

import Foundation

if CommandLine.argc < 3 {
    let executableName = (CommandLine.arguments[0] as NSString).lastPathComponent
    
    print("Usage:")
    print("\(executableName) [Source File] [Asset File] -t [type]")
    print("")
    print("Types:")
    print("beta, demo, dev, qa, sit, uat, release (default)")
    
    exit(EXIT_FAILURE)
}

let sourceString = CommandLine.arguments[1]
let assetsString = CommandLine.arguments[2]
let type: BuildType

if CommandLine.argc >= 5 {
    type = BuildType(value: CommandLine.arguments[4].lowercased())
} else {
    type = .release
}

let generator = Generator(source: sourceString, assets: assetsString, type: type)

do {
    try generator.generate()
} catch {
    print("Error generating images")
    exit(EXIT_FAILURE)
}
